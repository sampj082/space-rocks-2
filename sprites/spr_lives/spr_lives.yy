{
    "id": "d1ca574a-2629-4841-8169-c5a2459030cc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_lives",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 18,
    "bbox_left": 3,
    "bbox_right": 19,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5ed242c1-ff3d-484f-b9d7-32dcf850fceb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d1ca574a-2629-4841-8169-c5a2459030cc",
            "compositeImage": {
                "id": "ff8b47eb-6b9a-4d64-aac1-e807d4b3c657",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ed242c1-ff3d-484f-b9d7-32dcf850fceb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6e911fa4-0ed1-4983-bd78-0067beec9f65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ed242c1-ff3d-484f-b9d7-32dcf850fceb",
                    "LayerId": "b24f508e-f10d-40aa-b4c0-4190a3f91a9c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "b24f508e-f10d-40aa-b4c0-4190a3f91a9c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d1ca574a-2629-4841-8169-c5a2459030cc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}