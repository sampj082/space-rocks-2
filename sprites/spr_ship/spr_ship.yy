{
    "id": "fb8e96a6-0cf4-4f8b-9089-7782e40a576e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ship",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 20,
    "bbox_left": -1,
    "bbox_right": 31,
    "bbox_top": 9,
    "bboxmode": 2,
    "colkind": 3,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c743ec77-83f7-4638-90a2-9dd7abfcad14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb8e96a6-0cf4-4f8b-9089-7782e40a576e",
            "compositeImage": {
                "id": "d8231d15-6378-497f-95fd-90d74b7f8771",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c743ec77-83f7-4638-90a2-9dd7abfcad14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "137d832a-55ed-4183-ac83-aaed4136d42b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c743ec77-83f7-4638-90a2-9dd7abfcad14",
                    "LayerId": "fa06bb2a-943a-42ff-b39f-9b7310665ac1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "fa06bb2a-943a-42ff-b39f-9b7310665ac1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fb8e96a6-0cf4-4f8b-9089-7782e40a576e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 15
}