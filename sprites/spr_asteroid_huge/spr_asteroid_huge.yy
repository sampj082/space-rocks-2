{
    "id": "cc7bbb22-1813-4671-8a62-d7e1e4795e37",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_asteroid_huge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 4,
    "bbox_right": 57,
    "bbox_top": 5,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fc9c6b7c-528a-4dc5-b6a8-feefef1f09af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cc7bbb22-1813-4671-8a62-d7e1e4795e37",
            "compositeImage": {
                "id": "3a00131d-fc5b-458a-b9c9-e025f41ff897",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc9c6b7c-528a-4dc5-b6a8-feefef1f09af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "22ac9f3a-1a68-4050-8927-715506def73c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc9c6b7c-528a-4dc5-b6a8-feefef1f09af",
                    "LayerId": "70217d2a-3900-4cb5-a780-0dcef72c1012"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "70217d2a-3900-4cb5-a780-0dcef72c1012",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cc7bbb22-1813-4671-8a62-d7e1e4795e37",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}