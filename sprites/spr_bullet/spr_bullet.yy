{
    "id": "3d94d428-dc50-4906-b453-ab52f42d820c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1,
    "bbox_left": 0,
    "bbox_right": 1,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f3de6b5d-f101-453e-8204-7d8c5dda598a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3d94d428-dc50-4906-b453-ab52f42d820c",
            "compositeImage": {
                "id": "83c08c6d-2120-424b-b6a8-428e83ae438b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3de6b5d-f101-453e-8204-7d8c5dda598a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b458cf87-b823-4b90-8f6e-1a370b0b84d5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3de6b5d-f101-453e-8204-7d8c5dda598a",
                    "LayerId": "159e55f6-08b6-4dbe-aeb0-6538913ae54c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 2,
    "layers": [
        {
            "id": "159e55f6-08b6-4dbe-aeb0-6538913ae54c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3d94d428-dc50-4906-b453-ab52f42d820c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 2,
    "xorig": 1,
    "yorig": 1
}