{
    "id": "8bdb0664-1770-4a50-8416-0d18b71d244c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_asteroid_small",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "20bf16b4-ec16-4750-97ec-c68a52951d0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8bdb0664-1770-4a50-8416-0d18b71d244c",
            "compositeImage": {
                "id": "b077088c-dd24-4250-bbd0-95924c3308f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "20bf16b4-ec16-4750-97ec-c68a52951d0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7eae539c-8af2-4ca2-9cc5-e7847879148f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "20bf16b4-ec16-4750-97ec-c68a52951d0e",
                    "LayerId": "70f093f5-00c7-4d30-93f3-659d087b02e7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "70f093f5-00c7-4d30-93f3-659d087b02e7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8bdb0664-1770-4a50-8416-0d18b71d244c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}