{
    "id": "e96058b9-276e-4151-ba34-69115ee04807",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_debris",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f3654e4a-6014-4000-ab86-30671fcefc38",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e96058b9-276e-4151-ba34-69115ee04807",
            "compositeImage": {
                "id": "b9979f7a-e418-470f-8f92-f83d966c2a17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3654e4a-6014-4000-ab86-30671fcefc38",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b65c33c3-c23d-4639-8814-a7cc6cc7d965",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3654e4a-6014-4000-ab86-30671fcefc38",
                    "LayerId": "e0171c71-567b-4a08-b0de-24e722331979"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "e0171c71-567b-4a08-b0de-24e722331979",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e96058b9-276e-4151-ba34-69115ee04807",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}