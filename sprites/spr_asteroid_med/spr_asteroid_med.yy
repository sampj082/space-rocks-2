{
    "id": "36b4a96c-b153-4f4f-9093-82aed1fe639a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_asteroid_med",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 7,
    "bbox_right": 23,
    "bbox_top": 5,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4bd4d9b1-8741-41fe-9050-2a103a6910fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36b4a96c-b153-4f4f-9093-82aed1fe639a",
            "compositeImage": {
                "id": "a8139a0b-3843-41b6-a9eb-254a82f29d5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4bd4d9b1-8741-41fe-9050-2a103a6910fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7708465f-7265-418b-b22f-063e42b76fae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4bd4d9b1-8741-41fe-9050-2a103a6910fe",
                    "LayerId": "03adc86e-0e0a-47ba-a1b6-582cbf57e665"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "03adc86e-0e0a-47ba-a1b6-582cbf57e665",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "36b4a96c-b153-4f4f-9093-82aed1fe639a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}