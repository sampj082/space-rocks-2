{
    "id": "7e42f1f8-319b-4edd-a400-80a092301e47",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ship",
    "eventList": [
        {
            "id": "399aae96-d64e-4b06-bfb7-ab36bca26a0c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7e42f1f8-319b-4edd-a400-80a092301e47"
        },
        {
            "id": "9d6108da-e980-4553-9b01-29e5769a01dc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "34f79c4f-13ef-411e-8923-2cbac74856ae",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "7e42f1f8-319b-4edd-a400-80a092301e47"
        },
        {
            "id": "7e522955-59f2-4b59-8360-030917572fbf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 65,
            "eventtype": 5,
            "m_owner": "7e42f1f8-319b-4edd-a400-80a092301e47"
        },
        {
            "id": "2004d55f-97e0-468a-8e62-9db512b6f9da",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 87,
            "eventtype": 5,
            "m_owner": "7e42f1f8-319b-4edd-a400-80a092301e47"
        },
        {
            "id": "742ec266-8fe4-4a63-8b6c-ed5d56f24caf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 68,
            "eventtype": 5,
            "m_owner": "7e42f1f8-319b-4edd-a400-80a092301e47"
        },
        {
            "id": "7d29c5df-d26b-4027-be69-372c7cd88f94",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 9,
            "m_owner": "7e42f1f8-319b-4edd-a400-80a092301e47"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "fb8e96a6-0cf4-4f8b-9089-7782e40a576e",
    "visible": true
}