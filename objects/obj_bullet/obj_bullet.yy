{
    "id": "edb1a403-6562-4eee-b84c-4154bdde1cce",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullet",
    "eventList": [
        {
            "id": "36e28bc5-4cc0-40d3-b45e-5d0d1848abaf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "edb1a403-6562-4eee-b84c-4154bdde1cce"
        },
        {
            "id": "76fd1e9b-26f7-48a0-95d9-2b4a26d580ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "34f79c4f-13ef-411e-8923-2cbac74856ae",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "edb1a403-6562-4eee-b84c-4154bdde1cce"
        },
        {
            "id": "80a0d83f-33ae-4f29-905c-a19c650d7120",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "edb1a403-6562-4eee-b84c-4154bdde1cce"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3d94d428-dc50-4906-b453-ab52f42d820c",
    "visible": true
}