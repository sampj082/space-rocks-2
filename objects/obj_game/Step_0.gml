/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 574A409C
/// @DnDArgument : "var" "room"
/// @DnDArgument : "value" "rm_game"
if(room == rm_game)
{
	/// @DnDAction : YoYo Games.Instance Variables.If_Score
	/// @DnDVersion : 1
	/// @DnDHash : 4F58C542
	/// @DnDParent : 574A409C
	/// @DnDArgument : "op" "4"
	/// @DnDArgument : "value" "1000"
	if(!variable_instance_exists(id, "__dnd_score")) __dnd_score = 0;
	if(__dnd_score >= 1000)
	{
		/// @DnDAction : YoYo Games.Audio.Play_Audio
		/// @DnDVersion : 1
		/// @DnDHash : 6A82AD69
		/// @DnDParent : 4F58C542
		/// @DnDArgument : "soundid" "snd_win"
		/// @DnDSaveInfo : "soundid" "8c439148-2f5a-4d09-be12-2f3683da8c3d"
		audio_play_sound(snd_win, 0, 0);
	
		/// @DnDAction : YoYo Games.Rooms.Go_To_Room
		/// @DnDVersion : 1
		/// @DnDHash : 232C3830
		/// @DnDParent : 4F58C542
		/// @DnDArgument : "room" "rm_win"
		/// @DnDSaveInfo : "room" "251a1307-e62d-4cd4-a07d-20dd1f08356c"
		room_goto(rm_win);
	}

	/// @DnDAction : YoYo Games.Instance Variables.If_Lives
	/// @DnDVersion : 1
	/// @DnDHash : 100CDCE6
	/// @DnDParent : 574A409C
	/// @DnDArgument : "op" "3"
	if(!variable_instance_exists(id, "__dnd_lives")) __dnd_lives = 0;
	if(__dnd_lives <= 0)
	{
		/// @DnDAction : YoYo Games.Audio.Play_Audio
		/// @DnDVersion : 1
		/// @DnDHash : 23302194
		/// @DnDParent : 100CDCE6
		/// @DnDArgument : "soundid" "snd_lose"
		/// @DnDSaveInfo : "soundid" "7d5fd398-3814-42f9-9ac3-b98f72c49954"
		audio_play_sound(snd_lose, 0, 0);
	
		/// @DnDAction : YoYo Games.Rooms.Go_To_Room
		/// @DnDVersion : 1
		/// @DnDHash : 713E432B
		/// @DnDParent : 100CDCE6
		/// @DnDArgument : "room" "rm_gameover"
		/// @DnDSaveInfo : "room" "69d78baa-e3a0-415a-a950-86f32e34ded3"
		room_goto(rm_gameover);
	}
}