{
    "id": "bba6c9ab-c226-4ef9-af43-2506a17b5e65",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_debris",
    "eventList": [
        {
            "id": "9bdad9c0-aebf-4dd1-b0d7-23243289abb9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bba6c9ab-c226-4ef9-af43-2506a17b5e65"
        },
        {
            "id": "7fafdb53-ad16-4aec-b073-f77f44be465a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bba6c9ab-c226-4ef9-af43-2506a17b5e65"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e96058b9-276e-4151-ba34-69115ee04807",
    "visible": true
}